Разработка сервиса поиска нечетких высказываний в естественном языке (Fuzzy Logic)

Исполнитель: Лайд Егор Дмитриевич, студент ФКН ВШЭ ПМИ, группа 165-1

Цель проекта: развитие существующей программы формирования базы знаний правил нечеткой логики,
настроить взаимодействие программы с базами данных. На данный момент программа может создавать и
редактировать правила нечеткой логики (Fuzzy Rules). Так же, есть возможность вычислять, по заданному
входу, выходное значение на основе сохраненных правил нечеткой логики (Fuzzy Rules) посредством алгоритмов
Mamdani и/или Tagaki-Sugeno. Программаимеет пользовательский интерфейс и сохранение информации в виде файлов. 

Использование баз данных значительно увеличить объемы обрабатываемых программой данных, а так же позволит
структуризировать полученные результаты.

План работы:
1. Перевести программу на работу с базой данных. (выполнить до 12 марта)
2. Реализовать возможность выявлять из произвольного текста: правила нечеткой логики, лингвистические переменные,
термы, модифицированные термы, и сопоставлять с уже определенными в базе знаний. Поставлен эксперимент.
3. Произвести экспериментальную проверка программы, документировать функциональные возможности программы
