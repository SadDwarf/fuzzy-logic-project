from tkinter import *
import os
from DBfunctions import *
import MySQLdb
import numpy as np
import skfuzzy as fuzz

def center(window, size="600x400"):
    w = window.winfo_screenwidth()
    h = window.winfo_screendepth()
    window.geometry(size + "+" + str(w // 2) + "+" + str(h // 2))


def create_w(name):
    wind = Toplevel(main)
    wind.title(name)
    return wind

def call(lv):
    global VARIABLES
    e_mean = Entry(inpd, width=5)
    e_mean.grid(row=NUMBER, column=1)
    VARIABLES[-1][0] = lv
    VARIABLES[-1][1] = e_mean

def call2(lv):
    global PROBLEM
    PROBLEM = lv

def new_var():
    global inpd
    global NUMBER
    NUMBER += 1
    VARIABLES.append([0] * 2)
    file = open("list_of_lv.txt", "r")
    s = file.readlines()
    for i in range(len(s)):
        s[i] = s[i][:-1]
    file.close()
    var = StringVar()
    e_lv = OptionMenu(inpd, var, *s, command=call)
    e_lv.grid(row=NUMBER, column=0)

def new_var1():
    global inpd
    file = open("list_of_lv.txt", "r")
    s = file.readlines()
    for i in range(len(s)):
        s[i] = s[i][:-1]
    file.close()
    var = StringVar()
    e_lv = OptionMenu(inpd, var, *s, command=call2)
    e_lv.grid(row=1, column=2)


def convertToText(rules):
    file = open("suit_rules.txt", "w")
    file.truncate()
    for rule in rules:
        file.write("NEW_RULE 1\n")
        for i in range(4, len(rule), 3):
            if rule[i]:
                file.write("#IF #" + rule[i])
                if rule[i + 1]:
                    file.write(" #" + rule[i + 1])
                else:
                    file.write(" #-1")
                if rule[i + 2]:
                    file.write(" #" + rule[i + 2])
                else:
                    file.write(" #-1")
                file.write(" #\n")
        file.write("#THEN #" + rule[1])
        if rule[2]:
            file.write(" #" + rule[2])
        else:
            file.write(" #-1")
        if rule[3]:
            file.write(" #" + rule[3])
        else:
            file.write(" #-1")
        file.write(" #\n")
        file.write("END_RULE\n")



def accessDB():
    """
    Initializes connection with database and creates table if if doesnt exist
    """
    global CURSOR
    global connection
    try:
        connection = MySQLdb.connect(user="EgorLayd",
                                     passwd="1234",
                                     host="localhost",
                                     db="ProjectDB",)
    except:
        print("Can't connect to database")
    CURSOR = connection.cursor()
    initTable(CURSOR, TABLENAME)
    return CURSOR


def searchinDB():
    """
    Searches for matching rules in database
    """
    global PROBLEM
    global VARIABLES
    inquiry = [[None] * 3,]
    inquiry[0][0] = PROBLEM
    for i in VARIABLES:
        tmp = [None] * 3
        tmp[0] = i[0]
        inquiry.append(tmp)
    cur = accessDB()
    rules = findByCond(cur, TABLENAME, inquiry)
    convertToText(rules)

def solver(): #It's the version for simple statements (kind of "if x1 is A1 and ... and xn is An then z is B). That's bc further statements will be splitted in such parts
    global VARIABLES
    global PROBLEM
    vars = []
    searchinDB()
    for i in range(len(VARIABLES)):
        vars.append([0] * 2)
        vars[i][0] = VARIABLES[i][0]
        vars[i][1] = VARIABLES[i][1].get()
    #print(vars)
    had_vars = []
    for i in vars:
        had_vars.append(i[0])
    file = open("rules.txt")
    file2 = open("suit_rules.txt")
    lines = file.readlines() + file2.readlines()
    flag = "NILL"
    suit_rules = [] #rules with inputted variables only and PROBLEM in the right part
    for s in lines:
        if s.startswith("NEW_RULE"):
            flag = "NEW_RULE"
            vars_if = []
            var_then = ()
            c = s.split()[1]
            coeff = 1
            if c != "":
                coeff = float(c)
        if s.startswith("END_RULE"):
            flag = "NILL"
            suit = False
            if PROBLEM == var_then[0]:
                suit = True
                for var in vars_if:
                    if var[0] not in had_vars:
                        suit = False
                        break
                if suit:
                    suit_rules.append((vars_if, var_then, coeff))
        if (flag == "NEW_RULE"):
            words = s.split()
            if words[0] == "#IF":
                vars_if.append((words[1][1:], words[3][1:], words[2][1:]))
            if words[0] == "#THEN":
                var_then = (words[1][1:], words[3][1:], words[2][1:])
    #print(suit_rules)
    had_vars = dict()
    for var in vars:
        had_vars[var[0]] = int(var[1])
    ### rule pocessing
    fxs = []
    for rule in suit_rules:
        vars_if = rule[0]
        var_then = rule[1]
        coeff = rule[2]

        ### truncation(choose min)
        mini = 2
        for var in vars_if:
            name = var[0]
            term = var[1]
            mod = var[2]
            file = name + ".txt"
            file = open(file, "r")
            lines = file.readlines()
            for i in range(len(lines)):
                if lines[i].startswith("#" + term):
                    i += 1
                    func = lines[i].split()[0]
                    parms = list(map(int, lines[i].split()[1:]))
            file.close()
            if func == "const":
                point = had_vars[name]
                mean = 0
                if (point >= parms[0] and point <= parms[1]):
                    mean = 1
            if func == "tria":
                point = had_vars[name]
                y = fuzz.trimf(np.array([point]), parms)
                mean = y[0]
            if  func == "trap":
                point = had_vars[name]
                y = fuzz.trapmf(np.array([point]), parms)
                mean = y[0]
            ### use modification
            if mod == "NOT":
                mean = 1 - mean
            if mod == "POORLY":
                mean = mean * mean
            if mod == "VERY":
                mean = np.sqrt(mean)
            mini = min(mini, mean)
        LOW = -1000
        HIGH = 1000
        x = np.arange(LOW, HIGH, 1)
        name = var_then[0]
        term = var_then[1]
        mod = var_then[2]

        file = name + ".txt"
        file = open(file, "r")
        lines = file.readlines()
        for i in range(len(lines)):
            if lines[i].startswith("#" + term):
                i += 1
                func = lines[i].split()[0]
                parms = list(map(int, lines[i].split()[1:]))
        file.close()

        if func == "tria":
            fx = fuzz.trimf(x, parms)
        elif func == "trap":
            fx = fuzz.trapmf(x, parms)
        elif func == "const":
            fx = np.zeros_like(x)
            fx[parms[0] - LOW:parms[1] - LOW + 1] = 1

        ### modification
        if mod == "NOT":
            for i in range(len(fx)):
                fx[i] = 1 - fx[i]
        if mod == "POORLY":
            for i in range(len(fx)):
                fx[i] = fx[i] * fx[i]
        if mod == "VERY":
            for i in range(len(fx)):
                fx[i] = np.sqrt(fx[i])
        fx = np.fmin(mini, fx)
        fx = fx * rule[-1] #coeff
        fxs.append(fx)
    res = np.zeros_like(x)
    for fx in fxs:
        res = np.fmax(fx, res)
    res = fuzz.defuzz(x, res, 'centroid')
    print(res)




def input_data():
    global inpd
    global NUMBER
    global VARIABLES
    global PROBLEM
    VARIABLES = []
    NUMBER = 0
    inpd = create_w("Input data")
    inpd.withdraw()
    inpd.tkraise()
    center(inpd)
    inpd.deiconify()
    Button(inpd, text="Add variable", command=lambda: new_var()).grid(row=0, column=0)
    Button(inpd, text="To solve!", command=lambda: solver()).grid(row=0, column=1)
    Button(inpd, text="To know", command=lambda: new_var1()).grid(row=0, column=2)



global TABLENAME
TABLENAME = "linguistic_rules"

main = Tk()
center(main, "350x80")

Button(main, text = "Input data", command=input_data).grid(row = 2, column = 3)

mainloop( )
