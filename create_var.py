from tkinter import *
import os

def create_w(name):
    wind = Toplevel(main)
    wind.title(name)
    return wind

def center(window, size="600x400"):
    w = window.winfo_screenwidth()
    h = window.winfo_screendepth()
    window.geometry(size + "+" + str(w // 2) + "+" + str(h // 2))


def go_to_the_next(entry):
    entry.focus_force()


def write2(e_xs, t_name, lv_name):
    file = open(lv_name + ".txt", "r")
    file_s = file.read()
    file.close()
    if file_s.find("#" + t_name) != -1:
        i = file_s.find("#" + t_name)
        s_start = file_s[:i + len("#" + t_name + "\n")]
        s_end = file_s[i + len("#" + t_name + "\n"):]
        n = len(e_xs)
        xs = [-1] * n
        for i in range(n):
            xs[i] = e_xs[i].get()
        s = " ".join(xs)
        if n == 2:
            s = "const" + " " + s + "\n"
        elif n == 4:
            s = "trap" + " " + s + "\n"
        elif n == 3:
            s = "tria" + " " + s + "\n"
        file = open(lv_name + ".txt", "w")
        file.write(s_start + s + s_end)
        file.close()
    else:
        file = open(lv_name + ".txt", "a")
        file.write("#" + t_name + "\n")
        n = len(e_xs)
        xs = [-1] * n
        for i in range(n):
            xs[i] = e_xs[i].get()
        s = " ".join(xs)
        if n == 2:
            file.write("const" + " " + s + "\n")
        elif n == 4:
            file.write("trap" + " " + s + "\n")
        elif n == 3:
            file.write("tria" + " " + s + "\n")
        file.close()


def corr_xs(e_xs, t_name, lv_name):
    n = len(e_xs)
    xs = [-1] * n
    flag = True
    for i in range(n):
        x = e_xs[i].get()
        if len(x) == 0:
            flag = False
        for j in range(len(x)):
            if not x[j].isdigit():
                flag = False
        if flag:
            xs[i] = int(x)
            if (i != 0 and xs[i] <= xs[i - 1]):
                flag = False
    if not flag:
        error = create_w("Error")
        error.withdraw()
        error.tkraise()
        center(error, "150x50")
        error.deiconify()
        Label(error, text="Uncorrect meanings", font="Arial 12").pack()
    else:
        write2(e_xs, t_name, lv_name)

def corr1_xs(e_xs, t_name, lv_name):
    corr_xs(e_xs, t_name, lv_name)

def corr2_xs(e_xs, t_name, lv_name):
    corr_xs(e_xs, t_name, lv_name)

def corr3_xs(e_xs, t_name, lv_name):
    corr_xs(e_xs, t_name, lv_name)

def create_term(t_name, lv_name, lv):
    tm = create_w("Create term")
    tm.withdraw()
    tm.tkraise()
    center(tm, "700x400")
    tm.deiconify()
    Label(tm, text="Choose type").grid(row=0, column=0)
    canv = [-1] * 3
    colour = ["lightblue", "lightgreen", "lightgrey"]
    for i in range(3):
        canv[i] = Canvas(tm, width=200, height=100, bg="lightblue")
    for i in range(3):
        canv[i].grid(row=i + 1, column=0)
        canv[i].create_line(0 + 15, 100 - 15, 200 - 15, 100 - 15, width=3, fill="blue", arrow=LAST)
        canv[i].create_line(0 + 15, 100 - 15, 0 + 15, 0 + 15, width=3, fill="blue", arrow=LAST)
        canv[i].create_text(0 + 10, 50, text="1",  font="Arial 8")
    canv[0].create_text(80, 100 - 8, text="x1", font="Arial 8")
    canv[0].create_text(130, 100 - 8, text="x2", font="Arial 8")
    canv[0].create_line(80, 50, 130, 50, width=3, fill="black")

    x1 = 60; x2 = 80; x3 = 130; x4 = 160
    y_null = 100 - 15
    y_one = 50
    canv[1].create_text(x1, y_null + 7, text="x1", font="Arial 8")
    canv[1].create_text(x2, y_null + 7, text="x2", font="Arial 8")
    canv[1].create_text(x3, y_null + 7, text="x3", font="Arial 8")
    canv[1].create_text(x4, y_null + 7, text="x4", font="Arial 8")
    canv[1].create_line(x1, y_null, x2, y_one, width=3, fill="black")
    canv[1].create_line(x2, y_one, x3, y_one, width=3, fill="black")
    canv[1].create_line(x3, y_one, x4, y_null, width=3, fill="black")

    x1 = 80; x2 = 100; x3 = 130
    y_null = 100 - 15
    y_one = 50
    canv[2].create_text(x1, y_null + 7, text="x1", font="Arial 8")
    canv[2].create_text(x2, y_null + 7, text="x2", font="Arial 8")
    canv[2].create_text(x3, y_null + 7, text="x3", font="Arial 8")
    canv[2].create_line(x1, y_null, x2, y_one, width=3, fill="black")
    canv[2].create_line(x2, y_one, x3, y_null, width=3, fill="black")

    e_xs = [[-1] * 2, [-1] * 4, [-1] * 3]
    ind2 = 9
    for i in range(3):
        ind = 1
        if i == 0:
            counter = 2
        elif i == 1:
            counter = 4
        else:
            counter = 3
        for j in range(counter):
            Label(tm, text="x" + str(j + 1)).grid(row=i + 1, column=ind)
            ind += 1
            e_xs[i][j] = Entry(tm, width=10)
            e_xs[i][j].grid(row = i + 1, column=ind)
            ind += 1
        for j in range(counter - 1):
            e_xs[i][j].bind('<Tab>', lambda event: go_to_the_next(e_xs[i][j + 1]))
    but = [-1] * 3
    Button(tm, text="Done!", command=lambda: corr1_xs(e_xs[0], t_name, lv_name)).grid(row=0 + 1, column=ind2)
    Button(tm, text="Done!", command=lambda: corr2_xs(e_xs[1], t_name, lv_name)).grid(row=1 + 1, column=ind2)
    Button(tm, text="Done!", command=lambda: corr3_xs(e_xs[2], t_name, lv_name)).grid(row=2 + 1, column=ind2)

    types = ["const", "triangle", "trap"]



def input_points(k, lv_name, e_term, lv):
    index = 2
    global termnum
    term = [-1] * k
    for i in range(k):
        term[i] = e_term[i].get()
    b_term = [-1] * k
    for i in range(k):
        Label(lv, text=term[i]).grid(row=termnum + 2, column=3)
        b_term[i] = Button(lv, text=term[i], command=lambda: create_term(term[i], lv_name, lv))
        b_term[i].grid(row=termnum + 2, column=3)
        termnum += 2
        e_term[i].delete(0, len(e_term[i].get()))
        index += 1


def write1(e_name, k, e_term, lv):
    name = e_name.get()
    file = open(name + ".txt", "a")
    if os.stat(name + ".txt").st_size == 0:
        file.write(name + "\n")
    file.close()
    file = open("list_of_lv.txt", "a+")
    file1 = open("list_of_lv.txt", "r")
    s = file1.readlines()
    if (name + '\n') not in s:
        file.write(name + "\n")
    #print(s)
    #print(name)
    s = []
    file.close()
    lv_name = name
    input_points(k, lv_name, e_term, lv)


def new_lv():
    global termnum
    termnum = 0
    lv = create_w("Create linguistic variable")
    lv.withdraw()
    lv.tkraise()
    center(lv)
    number_of_string = 0
    lv.deiconify()
    Label(lv, text="Name").grid(row=0, column=0)
    e_name = Entry(lv)
    e_name.grid(row=0, column=1)
    number_of_string += 1
    e_name.bind('<Return>', lambda event: go_to_the_next(e_term))
    number_of_string += 1
    Label(lv, text="term").grid(row=number_of_string, column=0)
    e_term = Entry(lv)
    e_term.grid(row=number_of_string, column=1)
    Button(lv, text="Create", command=lambda: write1(e_name, 1, [e_term], lv)).grid(row=12, column=12)

def edit_lv():
    lv = create_w("Create linguistic variable")
    lv.withdraw()
    lv.tkraise()
    center(lv)
    number_of_string = 1
    lv.deiconify()
    Label(lv, text="Name").grid(row=1, column=0)
    e_name = Entry(lv)
    e_name.grid(row=1, column=1)
    e_name.bind('<Return>', lambda event: go_to_the_next(e_term))
    number_of_string += 1
    Label(lv, text="term").grid(row=number_of_string, column=0)
    e_term = Entry(lv)
    e_term.grid(row=number_of_string, column=1)
    Button(lv, text="Edit", command=lambda: write1(e_name, 1, [e_term], lv)).grid(row=12, column=12)




main = Tk()
center(main, "350x80")

Button(main, text = "Exit", command=main.quit).grid(row=2, column=2)


Button(main, text = "Create linguistic variable", command=new_lv).grid(row = 2, column = 3)
Button(main, text = "Edit", command=edit_lv).grid(row=2, column=4)




mainloop( )
