def initTable(cur, table_name):
    """
    Creates SQL query for table initializing if it does'nt exist and launches it.
    :param cur: pointer for DB cursor
    :type cur: MySQLdb.cursors.Cursor
    .. warning:: ."Table already exists"
    """
    conditionSize = "VARCHAR(30)"
    valueSize = "VARCHAR(30)"
    gradual = " ENUM('VERY', 'POORLY', 'NOT')"
    newSQLQuery = ("create table if not exists " + table_name +
                   " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                   "name VARCHAR(30)")
    newSQLQuery += ", valGrad "+ gradual + ", val " + valueSize
    for i in range(1, 4):
        newSQLQuery += ", cond" + str(i) + ' ' + conditionSize
        newSQLQuery += ", condGrad" + str(i) + ' ' + gradual
        newSQLQuery += ", condVal" + str(i) + ' ' + valueSize
    newSQLQuery += ");"
    print(newSQLQuery)
    cur.execute(newSQLQuery)
#0 - name, 1 - graduation, 2 - value


def appoint(cur, table_name, elem):
    """
    Creates SQL query for appending elements in DB and launches it.
    :param cur: pointer for DB cursor
    :type cur: MySQLdb.cursors.Cursor
    :param elem: rule in default presentation
    .. seealso:: default rule presentation
    """
    tmp = condNumber(cur, table_name)
    if tmp < len(elem) - 1:
        addRows(cur, table_name, tmp, len(elem) - 1)
    elem[1:] = sorted(elem[1:])
    newSQLQuery = "insert into " + table_name + " (id, name"
    if elem[0][1]:
        newSQLQuery += ", valGrad"
    if elem[0][2]:
        newSQLQuery += ", val"
    for i in range(1, len(elem)):
        newSQLQuery += ", cond" + str(i)
        if elem[i][1]:
            newSQLQuery += ", condGrad" + str(i)
        if elem[i][2]:
            newSQLQuery += ", condVal" + str(i)
    newSQLQuery += ") values (NULL, '"
    newSQLQuery += elem[0][0] + "'"
    if elem[0][1]:
        newSQLQuery += ", '" + elem[0][1] + "'"
    if elem[0][2]:
        newSQLQuery += ", '" + elem[0][2] + "'"
    for i in range(1, len(elem)):
        newSQLQuery += ", '" + elem[i][0] + "'"
        if elem[i][1]:
            newSQLQuery += ", '" + elem[i][1] + "'"
        if elem[i][2]:
            newSQLQuery += ", '" + elem[i][2] + "'"
    newSQLQuery += ");"
    #print(newSQLQuery)
    cur.execute(newSQLQuery)


def condNumber(cur, table_name):
    """
    Creates SQL query for counting columns in DB and launches it.
    :param cur: pointer for DB cursor
    :type cur: MySQLdb.cursors.Cursor
    :return: numer of condition columns in DB
    """
    newSQLQuery = "SELECT count(*) FROM information_schema.columns WHERE table_name = '" + table_name + "';"
    cur.execute(newSQLQuery)
    columns = cur.fetchall()[0]
    result = columns[0]
    result = int((result - 4) / 3)
    return result


def findByCond(cur, table_name, elem):
    """
    Creates SQL query for selecting items from DB and launches it.
    :param cur: pointer for DB cursor
    :type cur: MySQLdb.cursors.Cursor
    :param elem: rule in default presentation
    .. seealso:: default rule presentation
    :return: tuple of mathing rules
    """
    elem[1:] = sorted(elem[1:])
    newSQLQuery = "select * from " + table_name + " where"
    if (elem[0][0]):
        newSQLQuery += " name='" + elem[0][0] + "' and"
    if (elem[0][1]):
        newSQLQuery += " valGrad='" + elem[0][1] + "' and"
    if (elem[0][2]):
        newSQLQuery += " val='" + elem[0][2] + "' and"
    condDescription = (" cond", " condGrad", " condVal")
    for i in range(1, len(elem)):
        for j in range(3):
            if elem[i][j]:
                newSQLQuery += condDescription[j] + str(i) + "='" + elem[i][j] + "' and"
    newSQLQuery = newSQLQuery[:-4]
    newSQLQuery += ';'
    print(newSQLQuery)
    cur.execute(newSQLQuery)
    res = cur.fetchall()
    return res

def addRows(cur, table_name, old, new):
    """
    Creates SQL query for adding columns to DB and launches it.
    :param cur: pointer for DB cursor
    :type cur: MySQLdb.cursors.Cursor
    :param old: number of existing conditions columns
    :param new: number of new conditions columns
    """
    while old < new:
        old += 1
        newSQLQuery = "ALTER TABLE " + table_name + " ADD cond" + str(old) + " VARCHAR(30);"
        print(newSQLQuery)
        cur.execute(newSQLQuery)
        newSQLQuery = "ALTER TABLE " + table_name + " ADD condGrad" + str(old) + " ENUM('VERY', 'POORLY', 'NOT');"
        cur.execute(newSQLQuery)
        newSQLQuery = "ALTER TABLE " + table_name + " ADD condVal" + str(old) + " VARCHAR(30);"
        cur.execute(newSQLQuery)
