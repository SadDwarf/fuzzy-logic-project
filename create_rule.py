from tkinter import *
from DBfunctions import *
import MySQLdb
import os

def center(window, size="600x400"):
    w = window.winfo_screenwidth()
    h = window.winfo_screendepth()
    window.geometry(size + "+" + str(w // 2) + "+" + str(h // 2))


def create_w(name):
    wind = Toplevel(main)
    wind.title(name)
    return wind

def call2(term):
    global rl
    global COUNT
    global NUMBER
    global RULES
    RULES[-1][3] = term


def get_mean(e_start, index):
    global RULES
    start = e_start.curselection()
    if (start != ()):
        start = start[0]
        RULES[-1][index] = start


def call(lv):
    global rl
    global NUMBER
    global RULES
    RULES[-1][1] = lv
    if RULES[-1][-1] != -1:
        e_terms = RULES[-1][2]
        e_terms.destroy()
        RULES.pop()
    RULES[-1][-1] = 0
    file = open(lv + ".txt", "r")
    var = StringVar()
    s1 = file.readlines()
    s = []
    for i in range(len(s1)):
        if (s1[i][0] == "#"):
            s.append(s1[i][1:-1])

    e_terms = OptionMenu(rl, var, *s, command=call2)
    e_terms.grid(row=NUMBER, column=4)
    RULES[-1][3] = e_terms




def new_part():
    global rl
    global NUMBER
    global RULES
    NUMBER += 1
    RULES.append([-1] * 6)
    RULES[-1][-1] = -1
    if NUMBER == 1:
        text = "IF"
        e_start = Label(rl, text="IF")
    else:
        text = "IF"
        e_start = Label(rl, text="AND")
    RULES[-1][0] = text
    RULES[-1][4] = "AND"

    e_start.grid(row=NUMBER, column=0)
    file = open("list_of_lv.txt", "r")
    s = file.readlines()
    for i in range(len(s)):
        s[i] = s[i][:-1]
    file.close()
    var = StringVar()
    e_lv = OptionMenu(rl, var, *s, command=call)
    RULES[-1][1] = e_lv
    e_lv.grid(row=NUMBER, column=1)
    Label(rl, text="=").grid(row=NUMBER, column=2)

    e_mods = Listbox(rl, height=4, width=4, bg="lightgrey", selectbackground="grey", exportselection=0)
    e_mods.grid(row=NUMBER, column=3)
    e_mods.bind("<<ListboxSelect>>", lambda event: get_mean(e_mods, 2))
    for item in ["", "NOT", "VERY", "POORLY"]:
        e_mods.insert(END, item)

def write(RULES):
    global e_coeff
    coeff = e_coeff.get()
    if not(coeff!= "" and 0 <= float(coeff) and float(coeff) <= 1):
        coeff = "1"
    file = open("rules.txt", "a")
    file.write("NEW_RULE" + ' ' + coeff + "\n")
    starts = ["", "IF", "THEN"]
    conns = ["", "AND", "OR"]
    mods = ["", "NOT", "VERY", "POORLY"]
    for i in RULES:
        x = i[0]
        if x != -1:
            pass
            #x = starts[x]
        z = i[2]
        if z != -1:
            z = mods[z]
        y = i[4]
        if y != -1:
            pass
            #y = conns[y]
        s = "#" + str(x) + ' ' + "#" + str(i[1]) + ' ' + "#" + str(z) + ' ' + "#" + str(i[3]) +  ' ' + "#" + str(y) + "\n"
        file.write(s)
    file.write("END_RULE\n")
    file.close()

def new__rightpart():
    global rl
    global NUMBER
    global RULES
    NUMBER += 1
    RULES.append([-1] * 6)
    RULES[-1][-1] = -1
    e_start = Label(rl, text="THEN")
    RULES[-1][0] = "THEN"
    RULES[-1][4] = ""
    if len(RULES) > 1:
        RULES[-2][4] = ""
    e_start.grid(row=NUMBER, column=0)
    file = open("list_of_lv.txt", "r")
    s = file.readlines()
    for i in range(len(s)):
        s[i] = s[i][:-1]
    file.close()
    var = StringVar()
    e_lv = OptionMenu(rl, var, *s, command=call)
    RULES[-1][1] = e_lv
    e_lv.grid(row=NUMBER, column=1)
    Label(rl, text="=").grid(row=NUMBER, column=2)

    e_mods = Listbox(rl, height=4, width=4, bg="lightgrey", selectbackground="grey", exportselection=0)
    e_mods.grid(row=NUMBER, column=3)
    e_mods.bind("<<ListboxSelect>>", lambda event: get_mean(e_mods, 2))
    for item in ["", "NOT", "VERY", "POORLY"]:
        e_mods.insert(END, item)

def new_rule():
    global rl
    global NUMBER
    global RULES
    global e_coeff
    NUMBER = 0
    RULES = []
    rl = create_w("Create new rule")
    rl.withdraw()
    rl.tkraise()
    center(rl)
    rl.deiconify()
    Button(rl, text="Add left part", command=lambda: new_part()).grid(row=0, column=0)
    Button(rl, text="Add right", command=lambda: new__rightpart()).grid(row=0, column=1)
    Button(rl, text="Add rule", command=lambda: write(RULES)).grid(row=0, column=2)
    e_coeff = Entry(rl, width=5)
    Label(rl, text="Coefficient:").grid(row=0, column=3)
    e_coeff.grid(row=0, column=4)


def accessDB():
    """
    Initializes connection with database and creates table if if doesnt exist.
    """
    global CURSOR
    global connection
    try:
        connection = MySQLdb.connect(user="EgorLayd",
                                     passwd="1234",
                                     host="localhost",
                                     db="ProjectDB",)
    except:
        print("Can't connect to database")
    CURSOR = connection.cursor()
    print(type(CURSOR))
    initTable(CURSOR, TABLENAME)
    return CURSOR



def parseRules(): #eject rules from .txt file
    file = open("rules.txt", "r")
    s = file.readlines()
    inRule = False
    ans = []
    j = 1
    for str in s:
        i = str.split()
        if len(i) == 0:
            continue
        if i[0] == "NEW_RULE":
            inRule = True
            curRule = [[None] * 3,]
            continue
        elif i[0] == "END_RULE":
            ans.append(curRule)
            j = 1
            inRule = False
            continue
        if inRule:
            if i[0] == "#THEN":
                curRule[0][0] = i[1][1:]
                if i[2] != "#-1" and i[2] !="#":
                    curRule[0][1] = i[2][1:]
                if len(i[3]) > 1:
                    curRule[0][2] = i[3][1:]
            elif i[0] == "#IF":
                curRule.append([None] * 3)
                curRule[j][0] = i[1][1:]
                if i[2] != "#-1" and i[2] !="#":
                    curRule[j][1] = i[2][1:]
                if len(i[3]) > 1:
                    curRule[j][2] = i[3][1:]
                j += 1
            else:
                print("rules.txt parsing problem")
    return ans


def movetoDB():
    """
    Moves rules from .txt file to database
    """
    if not CURSOR:
        accessDB()
    rulesList = parseRules()
    for i in rulesList:
        appoint(CURSOR, TABLENAME, i)
    connection.commit()
    file = open("rules.txt", "w")
    file.truncate()


global TABLENAME
global CURSOR
CURSOR = None
TABLENAME = "linguistic_rules"

main = Tk()
center(main, "350x80")


Button(main, text = "Create new rule", command=new_rule).grid(row = 2, column = 3)
Button(main, text = "Move rules to DB", command=movetoDB).grid(row = 2, column = 4)

mainloop( )
